package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name= "users" , uniqueConstraints = {
		@UniqueConstraint(columnNames = {"EmailId"})
})
public class Users {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private long id;
	@Column(name="Name", nullable = false)
	private String Name;
	@Column(name="EmailId", nullable = false)
	private String EmailId;
	@Column(name="password", nullable = false)
	private String password;

}
