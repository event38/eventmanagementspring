package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class Register {
	
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name="firstName", nullable = false)
	private String firstName;
	@Column(name="lastName", nullable = false)
	private String lastName;
	@Column(name="EmailId", nullable = false)
	private String EmailId;
	@Column(name="Contact", nullable = false)
	private String Contact;
	@Column(name="Password", nullable = false)
	private String Password;


	



}