package com.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.EventSystem;

public interface EventManageRespoitory extends JpaRepository<EventSystem, Integer>{
	
	@Query("from EventSystem e where e.eventName= :eventName")
	public EventSystem getEventByName(@Param("eventName") String eventName);

}
