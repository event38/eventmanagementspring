package com.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.Register;

@Service
public class RegisterDAO {
	@Autowired
	RegisterRepository registerRepository;

	public List<Register> getAllRegister(){
		return registerRepository.findAll();
	}

	public Register getRegisterByclientId(int clientId) {
		Register reg=new Register();
		return registerRepository.findById(clientId).orElse(reg);
	}

	public Register getRegisterByfirstName(String firstName) {
		Register reg=new Register();

		Register register=registerRepository.getRegisterByfirstName(firstName);
		if(register != null) {
			return register;
		}
		return reg;

	}
	public Register registerClient(Register reg) {
		return registerRepository.save(reg);
	}

	public void deleteRegister(int clientId) {
		registerRepository.deleteById(clientId);
	}
	public Register login(String emailId, String password) {
		return registerRepository.login(emailId,password);
	}

}
