package com.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.EventSystem;

@Service
public class EventSystemDAO {
	@Autowired
	EventManageRespoitory eventmanageRepository;
	
	public List<EventSystem> getAllEventManage(){
		return eventmanageRepository.findAll();
	}
	public EventSystem getEventManageById(int eventId) {
		return eventmanageRepository.findById(eventId).orElse(new EventSystem());
	}
	public EventSystem getEventManageByName(String eventName) {
		EventSystem eventmanage=eventmanageRepository.getEventByName(eventName);
		
		if(eventmanage != null) {
			return eventmanage;
		}

		return new EventSystem();
	}
	public EventSystem registerEventManage(EventSystem event) {
		return eventmanageRepository.save(event);
	}
	public void deleteEventManage(int eventId) {
		eventmanageRepository.deleteById(eventId);
	}
}
