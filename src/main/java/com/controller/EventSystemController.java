package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.DAO.EventSystemDAO;
import com.dto.EventSystem;

@RestController
public class EventSystemController {
	
	@Autowired
	EventSystemDAO eventDAO;
	
	@GetMapping("/getAllEventManage")
	public List<EventSystem> getAllEvents(){
		return eventDAO.getAllEventManage();
	}
	
	@GetMapping("/getEventManageById/{id}")
	public EventSystem getRegisterById(@PathVariable("id") int eventId) {
		return eventDAO.getEventManageById(eventId);
	}
	
	@GetMapping("/getEventManageByName/{name}")
	public EventSystem getEventManageByName(@PathVariable("name") String eventName) {
		return eventDAO.getEventManageByName(eventName);
	}
	
	@PostMapping("/registerEventManage")
	public EventSystem registerEventManage(@RequestBody EventSystem event) {
		return eventDAO.registerEventManage(event);
	}
	
	@PutMapping("/updateEventManage")
	public EventSystem updateEventManage(@RequestBody EventSystem event) {
		return eventDAO.registerEventManage(event);
	}
	
	@DeleteMapping("/deleteEventManage/{id}")
	public String deleteEventManage(@PathVariable("id") int eventId) {
		eventDAO.deleteEventManage(eventId);
		return "Event Id is Deleted";
	}
	
	@GetMapping("registerEventSHC")
	public String registerEventHc() {
		EventSystem event1= new EventSystem( );
		eventDAO.registerEventManage(event1);
		return "Event Registered sucessfully";
		}
	
	

}
