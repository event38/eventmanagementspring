package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.DAO.EventSystemDAO;
import com.DAO.RegisterDAO;
import com.dto.Register;

@RestController
public class RegisterController {
	@Autowired
	RegisterDAO registerDAO;
	
	@Autowired
	EventSystemDAO eventDAO;
	
	@GetMapping("/getAllRegisters")
	public List<Register> getAllRegisters(){
		return registerDAO.getAllRegister();
	}
	
	@GetMapping("/getRegisterById/{id}")
	public Register getRegisterById(@PathVariable("id") int clientId) {
		return registerDAO.getRegisterByclientId(clientId);
	}
	@GetMapping("/getRegisterByName/{name}")
	public Register getRegisterByName(@PathVariable("name") String firstName) {
		return registerDAO.getRegisterByfirstName(firstName);
	}
	@GetMapping("/login/{emailId}/{password}")
	public Register login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		return registerDAO.login(emailId, password);
	}
	@PostMapping("/registerClient")
	public Register registerClient(@RequestBody Register reg) {
		return registerDAO.registerClient(reg);
	}
	
	@PutMapping("/updateClient")
	public Register updateClient(@RequestBody Register reg) {
		return registerDAO.registerClient(reg);
	}
	
	@DeleteMapping("/deleteClient")
	public String deleteClient(@PathVariable ("id") int clientId) {
		registerDAO.deleteRegister(clientId);
		return "Client Record Deleted";
	}
	
	@GetMapping("/registerClientsHC")
	public String registerClientHC() {
		Register reg1= new Register();
		
		
		List<Register> regList= registerDAO.getAllRegister();
		registerDAO.registerClient(reg1);
		return "Client Registered successfully";
	}
	
	
}
